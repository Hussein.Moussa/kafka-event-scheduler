package com.mryum.samples.delayed.events.processor;

import com.mryum.samples.delayed.events.domain.CustomerProfile;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantRecommendationResult;
import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.ScheduledEventTriggered;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.binder.kafka.streams.annotations.KafkaStreamsStateStore;
import org.springframework.cloud.stream.binder.kafka.streams.properties.KafkaStreamsStateStoreProperties;
import org.springframework.messaging.handler.annotation.SendTo;

/**
 * A Spring Cloud Streams helper library to make it easy to write a Kafka streams code.
 * It takes care of the boiler plate code required to initialise and start a Kafka streams app.
 */
@Slf4j
@EnableBinding(EventStreamsListener.StreamProcessor.class)
public class EventStreamsListener {

    private final EventSchedulerTopologyBuilder eventSchedulerTopologyBuilder;
    private final CustomerPersonalisedRestaurantRecommendationTopologyBuilder customerPersonalisedRestaurantRecommendationTopologyBuilder;

    public EventStreamsListener(EventSchedulerTopologyBuilder eventSchedulerTopologyBuilder,
                                CustomerPersonalisedRestaurantRecommendationTopologyBuilder customerPersonalisedRestaurantRecommendationTopologyBuilder) {
        this.eventSchedulerTopologyBuilder = eventSchedulerTopologyBuilder;
        this.customerPersonalisedRestaurantRecommendationTopologyBuilder = customerPersonalisedRestaurantRecommendationTopologyBuilder;
    }

    @StreamListener
    @SendTo({"scheduledEventTriggered", "customerRestaurantRecommendation"})
    @KafkaStreamsStateStore(name = "DelayedEventStateStore", type = KafkaStreamsStateStoreProperties.StoreType.KEYVALUE,
            valueSerde = "com.mryum.samples.delayed.events.serdes.DelayedEventSerde")
    public KStream<?, ?>[] process(@Input("futureEventScheduled") KStream<String, FutureEventScheduled> futureEventScheduledStream,
                                 @Input("customerProfile") KTable<String, CustomerProfile> customerProfileTable) {
        var scheduledEventTriggeredStream = eventSchedulerTopologyBuilder.build(futureEventScheduledStream);
        var customerRecommendationStream = customerPersonalisedRestaurantRecommendationTopologyBuilder.build(futureEventScheduledStream, customerProfileTable);

        return new KStream[] {scheduledEventTriggeredStream, customerRecommendationStream};
    }

    public interface StreamProcessor {
        @Input("futureEventScheduled")
        KStream<?, ?> futureEventScheduled();

        @Input("customerProfile")
        KTable<?, ?> customerProfile();

        @Output("scheduledEventTriggered")
        KStream<?, ?> scheduledEventTriggered();

        @Output("customerRestaurantRecommendation")
        KStream<?, ?> customerRestaurantRecommendation();
    }
}
