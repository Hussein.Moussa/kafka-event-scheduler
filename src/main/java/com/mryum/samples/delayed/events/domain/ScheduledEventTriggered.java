package com.mryum.samples.delayed.events.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * An event that capture the fact of triggering the scheduled event when the scheduled time passed.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduledEventTriggered {
    private long originalScheduledTimestamp;
    private long actualRunTimestamp;
    private EventType eventType;
    //Did not use generics here to make it easy with Json.
    //It should support different types of event body
    private CustomerRestaurantVisit eventBody;
}
