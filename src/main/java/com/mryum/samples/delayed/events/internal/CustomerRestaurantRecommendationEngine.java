package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.CustomerProfileAndRestaurantVisit;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantRecommendationResult;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantVisit;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

public class CustomerRestaurantRecommendationEngine {
    public Optional<CustomerRestaurantRecommendationResult> runFor(CustomerProfileAndRestaurantVisit customerProfileAndRestaurantVisit) {
        CustomerRestaurantVisit customerRestaurantVisit = customerProfileAndRestaurantVisit.getCustomerRestaurantVisit();
        if (customerRestaurantVisit.getRatingStars() < 3 ||
                isNotRecentVisit(customerRestaurantVisit)) {
            return Optional.empty();
        }

        return Optional.of(new CustomerRestaurantRecommendationResult(customerRestaurantVisit.getRestaurantId(),
                recommendVisitTimestamp(customerRestaurantVisit),
                recommendDishName(customerRestaurantVisit)));
    }

    private String recommendDishName(CustomerRestaurantVisit customerRestaurantVisit) {
        return customerRestaurantVisit.getDishName();
    }

    private long recommendVisitTimestamp(CustomerRestaurantVisit customerRestaurantVisit) {
        return customerRestaurantVisit.getVisitTimestamp() + Duration.ofDays(10).toMillis();
    }

    private boolean isNotRecentVisit(CustomerRestaurantVisit visit) {
        var visitTimeDiffToNow = Instant.now().toEpochMilli() - visit.getVisitTimestamp();
        return visitTimeDiffToNow > Duration.ofDays(7).toMillis();
    }
}
