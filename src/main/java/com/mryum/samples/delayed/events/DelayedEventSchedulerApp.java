package com.mryum.samples.delayed.events;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelayedEventSchedulerApp {

	public static void main(String[] args) {
		SpringApplication.run(DelayedEventSchedulerApp.class, args);
	}

}