package com.mryum.samples.delayed.events.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerRestaurantRecommendationResult {
    private String restaurantId;
    private long recommendedVisitTimestamp;
    private String recommendedDishName;
}
