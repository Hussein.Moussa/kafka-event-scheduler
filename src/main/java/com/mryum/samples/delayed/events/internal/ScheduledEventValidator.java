package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;

import java.util.function.Predicate;

@FunctionalInterface
public interface ScheduledEventValidator extends Predicate<FutureEventScheduled> {
}
