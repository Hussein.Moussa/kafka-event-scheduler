package com.mryum.samples.delayed.events.domain;

import lombok.Value;
import org.apache.kafka.common.serialization.Serde;

/**
 * It is used to represent a Kafka topic with the serdes required to serialise/deserialise the key and value of
 * events/records in that topic.
 *
 * @param <K> Key
 * @param <V> Value
 */
@Value
public class KafkaTopic<K, V> {
    String topicName;
    Serde<K> keySerde;
    Serde<V> valueSerde;

    public static <K, V> KafkaTopic<K, V> of(final String topicName, final Serde<K> keySerde,
                                             final Serde<V> valueSerde) {
        return new KafkaTopic<>(topicName, keySerde, valueSerde);
    }
}
