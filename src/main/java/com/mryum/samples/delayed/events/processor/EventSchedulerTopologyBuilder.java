package com.mryum.samples.delayed.events.processor;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.KafkaTopic;
import com.mryum.samples.delayed.events.domain.ScheduledEventTriggered;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.KStream;

import java.util.function.Supplier;

/**
 * The purpose of this builder is to separate the logic of building Kafka streams processing topology
 * from the specific syntax required by Spring-Cloud streams library.
 * That way, we could use full Kafka streams testing capabilities to write unit tests for the DSL and
 * the low level processor APIs of Kafka streams.
 */
@Slf4j
public class EventSchedulerTopologyBuilder {
    private final EventSchedulerContext eventSchedulerContext;
    private final Supplier<ScheduledEventProcessingTransformer> transformerSupplier;

    public EventSchedulerTopologyBuilder(EventSchedulerContext eventSchedulerContext, Supplier<ScheduledEventProcessingTransformer> transformerSupplier) {
        this.eventSchedulerContext = eventSchedulerContext;
        this.transformerSupplier = transformerSupplier;
    }

    KStream<String, ScheduledEventTriggered> build(final KStream<String, FutureEventScheduled> delayedEventStream) {
        return delayedEventStream.transform(transformerSupplier::get,
                eventSchedulerContext.scheduledEventStateStore);
    }

    @Value
    public static class EventSchedulerContext {
        String scheduledEventStateStore;
        KafkaTopic<String, FutureEventScheduled> delayedEventInputTopic;
    }
}
