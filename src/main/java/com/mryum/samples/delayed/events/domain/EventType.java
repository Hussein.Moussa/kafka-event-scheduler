package com.mryum.samples.delayed.events.domain;

public enum EventType {
    SendRestaurantVisitThanksEmail,
    SendFeedbackReminderEmail
}
