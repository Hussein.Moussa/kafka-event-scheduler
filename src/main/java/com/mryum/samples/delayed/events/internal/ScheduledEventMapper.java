package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.ScheduledEventTriggered;
import lombok.NonNull;

public class ScheduledEventMapper {
    public ScheduledEventTriggered apply(long actualExecutionTimestamp,
                                         @NonNull FutureEventScheduled futureEventScheduled) {
        return new ScheduledEventTriggered(futureEventScheduled.getScheduleTimestamp(), actualExecutionTimestamp,
                futureEventScheduled.getEventType(), futureEventScheduled.getEventBody());
    }
}
