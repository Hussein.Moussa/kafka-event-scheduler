package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import lombok.NonNull;

import java.time.Instant;

public class DefaultScheduledEventValidator implements ScheduledEventValidator {
    @Override
    public boolean test(@NonNull FutureEventScheduled futureEventScheduled) {
        return futureEventScheduled.getScheduleTimestamp() > Instant.now().toEpochMilli();
    }
}
