package com.mryum.samples.delayed.events.processor;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.ScheduledEventTriggered;
import com.mryum.samples.delayed.events.internal.ScheduledEventMapper;
import com.mryum.samples.delayed.events.internal.ScheduledEventValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Slf4j
public class ScheduledEventProcessingTransformer implements Transformer<String, FutureEventScheduled, KeyValue<String, ScheduledEventTriggered>> {
    private KeyValueStore<String, FutureEventScheduled> futureScheduledEventStateStore;
    private ProcessorContext context;
    private final String stateStoreName;
    private final int schedulerRunIntervalInSeconds;
    private final ScheduledEventMapper eventMapper;
    private final ScheduledEventValidator scheduledEventValidator;

    public ScheduledEventProcessingTransformer(String stateStoreName, int schedulerRunIntervalInSeconds,
                                               ScheduledEventMapper eventMapper,
                                               ScheduledEventValidator scheduledEventValidator) {
        this.stateStoreName = stateStoreName;
        this.schedulerRunIntervalInSeconds = schedulerRunIntervalInSeconds;
        this.eventMapper = eventMapper;
        this.scheduledEventValidator = scheduledEventValidator;
    }

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        futureScheduledEventStateStore = (KeyValueStore<String, FutureEventScheduled>) this.context.getStateStore(stateStoreName);
        context.schedule(Duration.ofSeconds(schedulerRunIntervalInSeconds),
                PunctuationType.WALL_CLOCK_TIME, this::processScheduledEvents);
    }

    @Override
    public KeyValue<String, ScheduledEventTriggered> transform(String key, FutureEventScheduled value) {
        if ( !scheduledEventValidator.test(value) ) {
            log.error("The event with key {} is invalid, it is going to be skipped", key);
            return null;
        }

        futureScheduledEventStateStore.put(key, value);
        log.info("Scheduling a delayed event for key {} at time {}",
                key, toDateTime(value.getScheduleTimestamp()));

        //This means that no records will be pushed to downstream nodes of the topology.
        //It is like end of processing the event
        return null;
    }

    private void processScheduledEvents(long currentTimestamp) {
        var entriesIterator = futureScheduledEventStateStore.all();
        while (entriesIterator.hasNext()) {
            var scheduledEventEntry = entriesIterator.next();
            if (isScheduledTimePassed(currentTimestamp, scheduledEventEntry.value)) {
                triggerScheduledEvent(currentTimestamp, scheduledEventEntry);
            }
        }
        context.commit();
    }

    private void triggerScheduledEvent(long currentTimestamp, KeyValue<String, FutureEventScheduled> scheduledEventEntry) {
        //This will forward the ScheduledEventTriggered to the downstream nodes of the topology when the time passed
        context.forward(scheduledEventEntry.key, eventMapper.apply(currentTimestamp, scheduledEventEntry.value));
        futureScheduledEventStateStore.delete(scheduledEventEntry.key);
        log.info("The delayed event with key {} that was scheduled to be processed at {} has been triggered at time {}",
                scheduledEventEntry.key, toDateTime(scheduledEventEntry.value.getScheduleTimestamp()),
                toDateTime(currentTimestamp));
    }

    private boolean isScheduledTimePassed(long currentTimestamp, FutureEventScheduled futureEventScheduled) {
        return futureEventScheduled.getScheduleTimestamp() <= currentTimestamp;
    }

    private ZonedDateTime toDateTime(long epochMillis) {
        return Instant.ofEpochMilli(epochMillis).atZone(ZoneId.of("Etc/UTC"));
    }

    @Override
    public void close() {

    }
}
