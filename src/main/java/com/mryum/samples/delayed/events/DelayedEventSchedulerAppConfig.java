package com.mryum.samples.delayed.events;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.KafkaTopic;
import com.mryum.samples.delayed.events.internal.CustomerRestaurantRecommendationEngine;
import com.mryum.samples.delayed.events.internal.DefaultScheduledEventValidator;
import com.mryum.samples.delayed.events.internal.ScheduledEventMapper;
import com.mryum.samples.delayed.events.processor.CustomerPersonalisedRestaurantRecommendationTopologyBuilder;
import com.mryum.samples.delayed.events.processor.EventSchedulerTopologyBuilder;
import com.mryum.samples.delayed.events.processor.EventSchedulerTopologyBuilder.EventSchedulerContext;
import com.mryum.samples.delayed.events.processor.ScheduledEventProcessingTransformer;
import com.mryum.samples.delayed.events.serdes.DelayedEventSerde;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Supplier;

@Configuration
public class DelayedEventSchedulerAppConfig {
    @Value("${eventScheduler.stateStoreName}")
    private String stateStoreName;
    @Value("${eventScheduler.schedulerRunIntervalInSeconds}")
    private int schedulerRunIntervalInSeconds;

    @Value("${spring.cloud.stream.bindings.futureEventScheduled.destination}")
    private String futureEventScheduledTopicName;

    @Bean
    public Serde<FutureEventScheduled> delayedEventSerde() {
        return new DelayedEventSerde();
    }

    @Bean
    public EventSchedulerTopologyBuilder eventSchedulerTopologyBuilder(Supplier<ScheduledEventProcessingTransformer> transformerSupplier) {
        var delayedEventInputKafkaTopic = new KafkaTopic<>(this.futureEventScheduledTopicName,
                new Serdes.StringSerde(), new DelayedEventSerde());
        return new EventSchedulerTopologyBuilder(
                new EventSchedulerContext(stateStoreName, delayedEventInputKafkaTopic), transformerSupplier);
    }

    @Bean
    public CustomerPersonalisedRestaurantRecommendationTopologyBuilder customerPersonalisedRestaurantRecommendationTopologyBuilder() {
        var recommendationEngine = new CustomerRestaurantRecommendationEngine();
        return new CustomerPersonalisedRestaurantRecommendationTopologyBuilder(recommendationEngine);
    }

    @Bean
    public Supplier<ScheduledEventProcessingTransformer> transformerSupplier() {
        return () -> new ScheduledEventProcessingTransformer(stateStoreName, schedulerRunIntervalInSeconds,
                new ScheduledEventMapper(), new DefaultScheduledEventValidator());
    }
}
