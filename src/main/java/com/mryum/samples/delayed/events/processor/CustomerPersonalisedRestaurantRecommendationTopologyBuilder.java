package com.mryum.samples.delayed.events.processor;

import com.mryum.samples.delayed.events.domain.*;
import com.mryum.samples.delayed.events.internal.CustomerRestaurantRecommendationEngine;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;

import java.util.Optional;

public class CustomerPersonalisedRestaurantRecommendationTopologyBuilder {
    private final CustomerRestaurantRecommendationEngine recommendationEngine;

    public CustomerPersonalisedRestaurantRecommendationTopologyBuilder(CustomerRestaurantRecommendationEngine recommendationEngine) {
        this.recommendationEngine = recommendationEngine;
    }

    KStream<String, CustomerRestaurantRecommendationResult> build(KStream<String, FutureEventScheduled> futureEventScheduledStream,
                                                   KTable<String, CustomerProfile> customerProfileTable) {
        //Assume this event type means customer had visited this restaurant before
        var customerRestaurantVisitStream = futureEventScheduledStream
                .filter((k, v) -> v.getEventType() == EventType.SendRestaurantVisitThanksEmail)
                .filter((k, v) -> v.getEventBody() != null)
                .selectKey((k, v) -> v.getEventBody().getCustomerId());

        var customerRestaurantVisitWithProfileStream =
                customerRestaurantVisitStream.join(customerProfileTable,
                        (v1, v2) -> new CustomerProfileAndRestaurantVisit(v2, v1.getEventBody()));

        return customerRestaurantVisitWithProfileStream
                .mapValues(recommendationEngine::runFor)
                .filter((k, v) -> v.isPresent())
                .mapValues(Optional::get);
    }
}
