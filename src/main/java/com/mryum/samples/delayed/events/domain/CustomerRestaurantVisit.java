package com.mryum.samples.delayed.events.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRestaurantVisit {
    private String customerId;
    private String restaurantId;
    private long visitTimestamp;
    private String dishName;
    private int ratingStars;
}

