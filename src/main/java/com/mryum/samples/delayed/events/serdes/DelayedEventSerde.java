package com.mryum.samples.delayed.events.serdes;

import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import org.springframework.kafka.support.serializer.JsonSerde;

public class DelayedEventSerde extends JsonSerde<FutureEventScheduled> {
    public DelayedEventSerde() {
        super(FutureEventScheduled.class);
    }
}
