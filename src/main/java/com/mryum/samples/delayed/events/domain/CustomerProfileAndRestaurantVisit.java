package com.mryum.samples.delayed.events.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerProfileAndRestaurantVisit {
    private CustomerProfile customerProfile;
    private CustomerRestaurantVisit customerRestaurantVisit;
}
