package com.mryum.samples.delayed.events.serdes;

import com.mryum.samples.delayed.events.domain.CustomerProfile;
import org.springframework.kafka.support.serializer.JsonSerde;

public class CustomerProfileSerde extends JsonSerde<CustomerProfile> {
    public CustomerProfileSerde() {
        super(CustomerProfile.class);
    }
}
