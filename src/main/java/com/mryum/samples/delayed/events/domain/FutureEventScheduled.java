package com.mryum.samples.delayed.events.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * An event/task/job to be scheduled for future processing.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FutureEventScheduled {
    private long scheduleTimestamp;
    private EventType eventType;
    //Did not use generics here to make it easy with Json.
    //It should support different types of event body
    private CustomerRestaurantVisit eventBody;
}
