package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.CustomerRestaurantVisit;
import com.mryum.samples.delayed.events.domain.EventType;
import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class DefaultScheduledEventValidatorTest {
    private final DefaultScheduledEventValidator target = new DefaultScheduledEventValidator();

    @Test
    void shouldReturnFalseScheduledTimeIsInThePastComparedToUtcCurrentTime() {
        var result = target.test(givenFutureEventScheduledAt(100));

        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenScheduledTimeIsInTheFuture() {
        var result = target.test(givenFutureEventScheduledAt(Instant.now().toEpochMilli() + 100));

        assertThat(result).isTrue();
    }

    private FutureEventScheduled givenFutureEventScheduledAt(long scheduleTimestamp) {
        return new FutureEventScheduled(scheduleTimestamp,
                EventType.SendRestaurantVisitThanksEmail,
                new CustomerRestaurantVisit("Customer-1", "Restaurant-1", System.currentTimeMillis(),
                        "Dish Name", 4));
    }
}