package com.mryum.samples.delayed.events.processor;

import com.mryum.samples.delayed.events.MockInMemorySerde;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantVisit;
import com.mryum.samples.delayed.events.domain.EventType;
import com.mryum.samples.delayed.events.domain.FutureEventScheduled;
import com.mryum.samples.delayed.events.domain.ScheduledEventTriggered;
import com.mryum.samples.delayed.events.internal.ScheduledEventMapper;
import com.mryum.samples.delayed.events.internal.ScheduledEventValidator;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.processor.MockProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ScheduledEventProcessingTransformerTest {
    private static final String STATE_STORE_NAME = "state-store-name";
    private static final String KEY = "key";
    private static final int SCHEDULER_RUN_INTERVAL_IN_SECONDS = 1;

    private MockProcessorContext processorContext;
    private KeyValueStore<String, FutureEventScheduled> stateStore;
    private ScheduledEventProcessingTransformer target;
    @Mock
    private ScheduledEventMapper eventMapper;
    @Mock
    private ScheduledEventValidator scheduledEventValidator;

    @BeforeEach
    void setUp() {
        when(scheduledEventValidator.test(any())).thenReturn(true);
        target = new ScheduledEventProcessingTransformer(STATE_STORE_NAME, SCHEDULER_RUN_INTERVAL_IN_SECONDS,
                eventMapper, scheduledEventValidator);
        processorContext = new MockProcessorContext();
        setupStateStore();

        target.init(processorContext);
    }

    @AfterEach
    void tearDown() {
        processorContext.resetForwards();
        processorContext.resetCommit();
        if(stateStore != null)
            stateStore.close();
        MockInMemorySerde.resetCache();
    }

    @Test
    void shouldStoreNewEventsInTheStateStoreAndReturnNullToDownstreamNodes() {
        FutureEventScheduled event = givenFutureEventScheduledAt(100);

        var result = target.transform(KEY, event);

        assertThat(stateStore.get(KEY)).isEqualTo(event);
        assertThat(result).isNull();
    }

    @Test
    void shouldOverrideEventWithNewerVersionForTheSameKey() {
        FutureEventScheduled event_version_1 = givenFutureEventScheduledAt(100);
        FutureEventScheduled event_version_2 = givenFutureEventScheduledAt(6666);

        target.transform(KEY, event_version_1);
        target.transform(KEY, event_version_2);

        assertThat(stateStore.get(KEY)).isEqualTo(event_version_2);
    }

    @Test
    void shouldSchedulePunctuateToCheckForScheduledEventsInTheStateStore() {
        //The transformer was already initialised in the before-each. So the punctuator was already configured
        var capturedPunctuator = processorContext.scheduledPunctuators().get(0);

        assertThat(capturedPunctuator.getIntervalMs()).isEqualTo(Duration.ofSeconds(SCHEDULER_RUN_INTERVAL_IN_SECONDS).toMillis());
        assertThat(capturedPunctuator.getType()).isEqualTo(PunctuationType.WALL_CLOCK_TIME);
    }

    @Test
    void punctuatorShouldForwardEventsWhenTheirScheduledTimePassedAndCommitProcessorContext() {
        var originalScheduledTimestamp = 100;
        var actualRunTimestamp = 101;
        var event = givenFutureEventScheduledAt(originalScheduledTimestamp);
        var scheduledEventTriggered = mock(ScheduledEventTriggered.class);
        target.transform(KEY, event);
        var capturedPunctuator = processorContext.scheduledPunctuators().get(0);
        when(eventMapper.apply(actualRunTimestamp, event)).thenReturn(scheduledEventTriggered);

        var punctuator = capturedPunctuator.getPunctuator();
        punctuator.punctuate(actualRunTimestamp);

        var forwardedRecords = processorContext.forwarded();
        assertThat(forwardedRecords).hasSize(1);
        var scheduledEventOutputKeyValue = forwardedRecords.get(0).keyValue();
        assertThat(scheduledEventOutputKeyValue.key).isEqualTo(KEY);
        assertThat(scheduledEventOutputKeyValue.value).isEqualTo(scheduledEventTriggered);
        assertThat(processorContext.committed()).isTrue();
    }

    @Test
    void punctuatorShouldNotForwardAnyEventsWhenEventScheduledTimesAreStillInTheFuture() {
        var originalScheduledTimestamp = 500;
        var actualRunTimestamp = 101;
        var event = givenFutureEventScheduledAt(originalScheduledTimestamp);
        target.transform(KEY, event);

        triggerPunctuatorAtTime(actualRunTimestamp);

        var forwardedRecords = processorContext.forwarded();
        assertThat(forwardedRecords).isEmpty();
    }

    @Test
    void shouldPropagateThrownExceptionToStopTheWholeProcessingTopology() {
        var originalScheduledTimestamp = 100;
        var actualRunTimestamp = 101;
        var event = givenFutureEventScheduledAt(originalScheduledTimestamp);
        when(eventMapper.apply(actualRunTimestamp, event)).thenThrow(RuntimeException.class);
        target.transform(KEY, event);

        assertThrows(RuntimeException.class, () -> triggerPunctuatorAtTime(actualRunTimestamp));
        assertThat(processorContext.committed()).isFalse();
    }

    @Test
    void shouldIgnoreInvalidScheduledEvents() {
        var event = givenFutureEventScheduledAt(100);
        when(scheduledEventValidator.test(event)).thenReturn(false);

        target.transform(KEY, event);

        assertThat(stateStore.get(KEY)).isNull();
    }

    private void triggerPunctuatorAtTime(int actualRunTimestamp) {
        var capturedPunctuator = processorContext.scheduledPunctuators().get(0);

        var punctuator = capturedPunctuator.getPunctuator();
        punctuator.punctuate(actualRunTimestamp);
    }

    private FutureEventScheduled givenFutureEventScheduledAt(long scheduleTimestamp) {
        return new FutureEventScheduled(scheduleTimestamp,
                EventType.SendRestaurantVisitThanksEmail,
                new CustomerRestaurantVisit("Customer-1", "Restaurant-1", System.currentTimeMillis(),
                        "Dish Name", 4));
    }

    private void setupStateStore() {
        stateStore =
                Stores.keyValueStoreBuilder(
                        Stores.inMemoryKeyValueStore(STATE_STORE_NAME),
                        Serdes.String(),
                        new MockInMemorySerde<FutureEventScheduled>())
                        .withLoggingDisabled()
                        .build();
        stateStore.init(processorContext, stateStore);
        processorContext.register(stateStore, null);
    }
}