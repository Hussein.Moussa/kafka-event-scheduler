package com.mryum.samples.delayed.events.internal;

import com.mryum.samples.delayed.events.domain.CustomerProfile;
import com.mryum.samples.delayed.events.domain.CustomerProfileAndRestaurantVisit;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantRecommendationResult;
import com.mryum.samples.delayed.events.domain.CustomerRestaurantVisit;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class CustomerRestaurantRecommendationEngineTest {

    private CustomerRestaurantRecommendationEngine target = new CustomerRestaurantRecommendationEngine();

    @Test
    void shouldReturnEmptyWhenCustomerRatingIsLessThan3Stars() {
        var customerVisit = givenCustomerVisitWith(2, 1);

        var result = target.runFor(customerVisit);

        assertThat(result).isEmpty();
    }

    @Test
    void shouldReturnEmptyWhenVisitTimeWasMoreThan7Days() {
        var customerVisit = givenCustomerVisitWith(4, 8);

        var result = target.runFor(customerVisit);

        assertThat(result).isEmpty();
    }

    @Test
    void shouldReturnRecommendVisitTimeOf10DaysInTheFutureFromTheLastVisitAndTheSameDishAsLastVisit() {
        var customerVisit = givenCustomerVisitWith(4, 1);

        var result = target.runFor(customerVisit);

        var recommendedVisitTime = customerVisit.getCustomerRestaurantVisit().getVisitTimestamp()
                + Duration.ofDays(10).toMillis();
        var expectedRecommendedResult = new CustomerRestaurantRecommendationResult(customerVisit.getCustomerRestaurantVisit().getRestaurantId(),
                recommendedVisitTime, customerVisit.getCustomerRestaurantVisit().getDishName());
        assertThat(result).hasValue(expectedRecommendedResult);
    }

    private CustomerProfileAndRestaurantVisit givenCustomerVisitWith(int ratingStars, int countDaysInThePast) {
        long visitTime = Instant.now().toEpochMilli() - Duration.ofDays(countDaysInThePast).toMillis();
        var customerVisit = new CustomerRestaurantVisit("Customer-1", "Restaurant-1", visitTime, "Dish-1", ratingStars);
        return new CustomerProfileAndRestaurantVisit(new CustomerProfile("Hussein", "Melbourne"), customerVisit);
    }
}