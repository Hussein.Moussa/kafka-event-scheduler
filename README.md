# Delayed event scheduler

One of the common tasks in microservices is scheduling events/tasks to be executed in the future. In this PoC I have used Kafka Streams to demonstrate how events in Kafka topics can be scheduled for future.

## Assumptions

* Services publish event of type `FutureEventScheduled` in a Kafka topic with details about the task to be executed in the future.
* Those delayed events should not be lost or not being sent when their scheduled time passed.
* Scheduler should support future scale, so it needs to support scaling up/down based on the load of scheduled events in a fully distributed and fault-tolerant way.
* In case of failure to process those scheduled events, the scheduler should retry. At the same time, it would be better to reduce the retries as it could result in undeseriable side effects. For example, sending multiple emails to the customer because of processing the same event by multiple service instances at the same time or processing retry.

## Why this approach?

Kafka does not support this feature natively like AWS SQS for example where we can set a visibility timeout to prevent the consumer from consuming those events/messages. Once an event lands in Kafka topic, consumers would be able to see and consume this event immediately. So, I used Kafka Streams's state store to keep track of those future event, and when its schedule time is passed, an event will be published to capture the fact that it is the time to action this event/task. It could be sending an email to the customer, or sending a request to
a third-party service, endless number of options to go with.
The following graph shows a high level of what the processing topology is doing on real-time

![Scheduled event processing topology](docs/DelayedEventSchedulerTopology.jpg)

## Design considerations

I chose Kafka Streams as it is a lightweight distributed real-time stream processing framework that could achieve exactly-once processing of events. And natively support scalable and fault-tolerant event processing.

* Used `exactly-once` processing guarantee which implies idempotent producer and Kafka transactions are used.
* Avoid doing the actual integration with external services(any thing other than Kafka topics) when the scheduled time passed. And instead publish an event of type `ScheduledEventTriggered` to another topic which could be consumed by a `Kafka Connector` to do the actual integration. This guarantees exactly-once processing and makes it easy to plug and play different connectors to integrate with different services with minmum changes.
* Based on the fact that the scheduler service can scale up to the number of partitions in the source topic, I used event ID as a key for the state store to keep the PoC simple. This means each instance of the  scheduler service would handle sub-set of the events.

## Risks and trade offs

* **Too much records in the state store**: this could be the case if we are talking about millions of events scheduled for long period. This means each scheduler instance might still need to search through millions of records in the state store each time the punctuator runs. To solve this issue, we could use the scheduled time as a key for the state store. And implement a custom version that supports searching for key range.
* **Spring Cloud Streams and big deployment artifact**: In this PoC I used Spring Cloud Streams to avoid writing the boilerplate code required to start Kafka Streams. Spring comes with so many dependencies. I chose this approach to save my time and focus on other use cases of the events.

# Use cases of events in the platform

Once we have all the events in one shared event-platform like Kafka, we can re-use the same events in so many use cases. In this PoC I tried to use the scheduled event and customer profile to run a recommendation engine to personalise restaurant recommendations based on previous customer behaviour. It is a simple use case, but might show the bright side of using event-streaming platform like Kafka.

## Personalised customer & restuarant recommendations

![Scheduled event processing topology](docs/PersonalisedRecommendations.jpg)

Lets assume that the scheduled event is of type `SendRestaurantVisitThanksEmail` which implies that customer had visited a retaurant. This event stream could be joined with Customer profile to enrich the visit and run a personalised recommendation algorithm to recommend the restaurants that user gave good ratings for recently.
The PoC is using a simple approach just to show the idea of reusing events in different use cases.

## Monitoring scheduled vs actual trigger time

One of the monitoring metrics we could use to monitor the delay of actually triggering the scheduled events vs the planned schedule time. Instead of adding code in the scheduler to export metrics of the time difference when publishing `ScheduledEventTriggered` event. We could have a separate montoring app that joins both events `FutureEventScheduled` and `ScheduledEventTriggered` to export such metrics. This way the logic of exporting metrics could be reused in different use cases.

# Alternative solutions

## Kafka Message Scheduler

Another implementation to solve the same issue using Kafka as well. For more details check [this repo](https://github.com/sky-uk/kafka-message-scheduler).

## Redis Delayed tasks

Redis supports scheduling delayed tasks to be triggered in the future. In [this repo](https://github.com/davidmarquis/redis-scheduler), we can find a Java implementation of Redis scheduler.

## AWS CloudWatch events

CloudWatch events supports definig a scheduled job to run and trigger Lambda function for example. Some considerations to consider in this approach

* Need to make sure non of the scheduled events will be lost in case the triggered Lambda for example failed.
* Does it support load of millions of events. And what is the cost involved.
* Need to find a way to make it idempotent so, event has somekind of unique ID so all updates or retries while creating the CloudWatch event would result in creating only one event.

# How to run

The following screenshot shows the result of running the app. The scheduled events gets triggered and even gets published to the `scheduled-event-triggered` topic. Also, the recommendation topology runs and push the result to topic `customer-restaurant-recommendation`

![Events in the Kafka topics](docs/RunScreenshot.png)

The steps to run
* Start Kafka & Zookeeper containers using `docker-compose up -d`
* Wait for the containers to start, and run the script to publish test events to the topics [Events in the Kafka topics](scripts/push_random_events.sh).
* Start the stream processing app using `gradle bootrun`