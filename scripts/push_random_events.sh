current_timestamp_in_seconds=$(date -u +%s)
future_gap_minutes=$(( ( ( RANDOM % 5 )  + 1 ) ))
event_schedule_time_epoch_millis=$(( (current_timestamp_in_seconds + (future_gap_minutes*60))*1000 ))
customerId='Customer_1'
restaurantId='Restaurant_1'
visitTimestamp=$(( event_schedule_time_epoch_millis - (5 * 60 * 60 * 1000) ))
dishName="Dish Name 1"
ratingStars=4
customer_full_name='Hussein Moussa'

customer_profile_event_body='{"fullName": "'${customer_full_name}'", "city": "Melbourne"}'

event_body='{"scheduleTimestamp": '${event_schedule_time_epoch_millis}', "eventType": "SendRestaurantVisitThanksEmail", "eventBody": {"customerId": "'${customerId}'", "restaurantId": "'${restaurantId}'", "visitTimestamp": '${visitTimestamp}', "dishName": "'${dishName}'", "ratingStars": '${ratingStars}'}}'

event_key='Event_'$RANDOM

docker exec -it broker echo "$customerId:$customer_profile_event_body" | kafka-console-producer --broker-list localhost:9092 --topic customer-profile --property "parse.key=true" --property "key.separator=:"
docker exec -it broker echo "$event_key:$event_body" | kafka-console-producer --broker-list localhost:9092 --topic future-event-scheduled --property "parse.key=true" --property "key.separator=:"
echo "The event with key ${event_key} is scheduled to run at ${event_schedule_time_epoch_millis}"

